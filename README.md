phpMyAdmin - Readme
===================

A web interface for MySQL and MariaDB.

Summary
-------

phpMyAdmin is intended to handle the administration of MySQL over the web.
For a summary of features, list of requirements, and installation instructions,
please see the documentation in the ./doc/ folder or at https://docs.phpmyadmin.net/
